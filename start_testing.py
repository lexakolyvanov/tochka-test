import unittest
import app_form

suite = unittest.TestSuite()

suite.addTests(unittest.TestLoader().loadTestsFromTestCase(app_form.TestPhoneForm))
testResult = unittest.TextTestRunner(verbosity=2).run(suite)

if testResult.errors:
    raise SystemExit(1)
