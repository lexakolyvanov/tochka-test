from unittest import TestCase
from selenium import webdriver
import os

class BaseTestCase(TestCase):

    def setUp(self):
        """Start web driver"""
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        if "USERAGENT" in os.environ:
            chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument(self.choose_user_agent())
        self.driver = webdriver.Chrome(chrome_options=chrome_options)
        self.driver.implicitly_wait(10)

    @staticmethod
    def choose_user_agent():
        if "USER-AGENT" in os.environ:
            if os.environ['USER-AGENT'] == 'ios':
                return "--user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, " \
                       "like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25 "

            elif os.environ['USER-AGENT'] == 'android':
                return "--user-agent=Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) " \
                       "AppleWebKit/535.19 (KHTML, like Gecko; googleweblight) Chrome/38.0.1025.166 Mobile " \
                       "Safari/535.19 "
        else:
            return "--user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, " \
                   "like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25 "

