import unittest
from selenium.webdriver.support.ui import WebDriverWait # Import at top of file
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from base_testcase import BaseTestCase

url = 'http://localhost:10888/'


class TestPhoneForm(BaseTestCase):

    def test_minimal_number(self):
        self.driver.get(url)

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@class='simple-input__input']")))

        title_field = self.driver.find_element_by_xpath("//input[@class='simple-input__input']")
        title_field.send_keys("0000000000")

        send_button = self.driver.find_element_by_xpath("//a[.='Отправить заявку']")
        send_button.click()

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.presence_of_element_located((By.XPATH, "//div[.='Заявка отправлена']")))

    def test_maximal_number(self):
        self.driver.get(url)

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@class='simple-input__input']")))

        title_field = self.driver.find_element_by_xpath("//input[@class='simple-input__input']")
        title_field.send_keys("9999999999")

        send_button = self.driver.find_element_by_xpath("//a[.='Отправить заявку']")
        send_button.click()

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.presence_of_element_located((By.XPATH, "//div[.='Заявка отправлена']")))

    def test_void_number(self):
        self.driver.get(url)

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@class='simple-input__input']")))

        send_button = self.driver.find_element_by_xpath("//a[.='Отправить заявку']")
        send_button.click()

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.presence_of_element_located((By.XPATH, "//div[.='заполните все']")))

    def test_letter_number(self):
        self.driver.get(url)

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@class='simple-input__input']")))

        title_field = self.driver.find_element_by_xpath("//input[@class='simple-input__input']")
        title_field.send_keys("abcdefghkl")

        send_button = self.driver.find_element_by_xpath("//a[.='Отправить заявку']")
        send_button.click()

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.presence_of_element_located((By.XPATH, "//div[.='заполните все']")))

    def test_symbol_number(self):
        self.driver.get(url)

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@class='simple-input__input']")))

        title_field = self.driver.find_element_by_xpath("//input[@class='simple-input__input']")
        title_field.send_keys("!..,@,,,..//")

        send_button = self.driver.find_element_by_xpath("//a[.='Отправить заявку']")
        send_button.click()

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.presence_of_element_located((By.XPATH, "//div[.='заполните все']")))

    def test_incomplete_number(self):
        self.driver.get(url)

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@class='simple-input__input']")))

        title_field = self.driver.find_element_by_xpath("//input[@class='simple-input__input']")
        title_field.send_keys("967909")

        send_button = self.driver.find_element_by_xpath("//a[.='Отправить заявку']")
        send_button.click()

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.presence_of_element_located((By.XPATH, "//div[.='заполните все']")))

    def test_digits_symbol_combination_number(self):
        self.driver.get(url)

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@class='simple-input__input']")))

        title_field = self.driver.find_element_by_xpath("//input[@class='simple-input__input']")
        title_field.send_keys("967909..nm")

        send_button = self.driver.find_element_by_xpath("//a[.='Отправить заявку']")
        send_button.click()

        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.presence_of_element_located((By.XPATH, "//div[.='заполните все']")))

    def tearDown(self):
        """Stop web driver"""
        self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCalendarToSearch)
    unittest.TextTestRunner(verbosity=2).run(suite)
